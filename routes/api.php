<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {

    Route::group(['middleware' => 'auth:api'], function () {
        
        Route::resource('role', 'RoleController', [
            'except' => ['create', 'edit']
        ]);
    
        Route::resource('user', 'UserController', [
            'except' => ['create', 'edit']
        ]);
    
        Route::resource('category', 'CategoryController', [
            'except' => ['create', 'edit']
        ]);
    
        Route::resource('menu', 'MenuController', [
            'except' => ['create', 'edit']
        ]);
    
        Route::post('test-upload', 'TestUploadController@index');
    
        Route::post('logout', 'Auth\LoginController@logout');

    });

    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Auth\LoginController@login');
});
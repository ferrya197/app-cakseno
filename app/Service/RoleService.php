<?php
namespace App\Service;

class RoleService extends BaseService implements IBaseService
{
    /**
     * @param $request
     * @return array
     */
    public function validation($request)
    {
        return $this->validate($request, [
            'role_name' => 'required',
            'access' => 'required',
        ]);
    }
}
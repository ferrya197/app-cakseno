<?php
namespace App\Service;

class CategoryService extends BaseService implements IBaseService
{
    /**
     * @param $request
     * @return array
     */
    public function validation($request)
    {
        return $this->validate($request, [
            'category_name' => 'required'
        ]);
    }
}
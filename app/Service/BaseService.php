<?php
namespace App\Service;

use Illuminate\Foundation\Validation\ValidatesRequests;

class BaseService
{
    use ValidatesRequests;

    public function createResponse($args)
    {
        $args += [
            "msg" => null,
            "data" => null
        ];

        $message = $args["msg"];
        $data = $args["data"];

        if($message == null && $data != null)
        {
            $response = [
                "message" => "Berhasil Menemukan Data",
                "data" => $data
            ];

            return $response;
        }
        else if($message != null && $data == null)
        {
            $response = [
                "message" => $message,
                "data" => "Data tidak ditemukan"
            ];

            return $response;
        }
        else
        {
            $response = [
                "message" => $message,
                "data" => $data
            ];

            return $response;
        }
    }

    public function checkDataIsExist($data)
    {
        if($data == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function makeResponse($data, $status = null, $table)
    {
        if($status == 'read')
        {
            return $this->createResponse([
                "msg" => "Berhasil Menampilkan Data ". $table,
                "data" => $data
            ]);
        }
        else if($status == 'create')
        {
            return $this->createResponse([
                "msg" => "Berhasil Membuat Data ". $table,
                "data" => $data
            ]);
        }
        else if($status == 'update')
        {
            return $this->createResponse([
                "msg" => "Berhasil Mengubah Data ". $table,
                "data" => $data
            ]);
        }
        else if($status == 'delete')
        {
            return $this->createResponse([
                "msg" => "Berhasil Menghapus Data ". $table,
                "data" => $data
            ]);
        }
        else if($status == 'error')
        {
            return $this->createResponse([
                "msg" => "Ada Sesuatu Yang Error",
                "data" => $data
            ]);
        }
        else if($status == 'forbidden update id')
        {
            return $this->createResponse([
                "msg" => "Tidak bisa mengedit ". $table . ", ID Terlindungi Oleh Sistem",
                "data" => $data
            ]);
        }
        else if($status == 'forbidden delete id')
        {
            return $this->createResponse([
                "msg" => "Tidak bisa Menghapus ". $table .", ID Terlindungi Oleh Sistem",
                "data" => $data
            ]);
        }
    }

    public function checkAndCreateResponse($check, $request, $status, $table)
    {
        if($check == true)
        {
            return $this->makeResponse($request, $status, $table);
        }
        else{
            return $this->makeResponse($request, 'error', $table);
        }
    }
}
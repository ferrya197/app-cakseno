<?php

namespace App\Service;

class UserService extends BaseService implements IUserService
{
    /**
        * @param $request
        * @return array
        */
    public function validation($request)
    {
        return $this->validate($request, [
            'role_id'       => 'required',
            'name'          => 'required',
            'email'         => 'required|email',
            'password'      => 'required|min:5'
        ]);
    }

    public function checkDetailUser($request)
    {
        if(isset($request))
        {
            return $request;
        }
        
        $detail = json_encode([
            'no_telp' => '',
            'address' => '',
            'tempat_lahir' => '',
            'tanggal_lahir' => ''
        ]);

        return json_decode($detail);
    }
}
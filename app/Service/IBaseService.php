<?php
namespace App\Service;

interface IBaseService
{
    public function validation($request);
}
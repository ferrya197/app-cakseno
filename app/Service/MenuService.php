<?php
namespace App\Service;

class MenuService extends BaseService implements IBaseService
{
    /**
     * @param $request
     * @return array
     */
    public function validation($request)
    {
        return $this->validate($request, [
            'category_id' => 'required',
            'menu_name' => 'required',
            'menu_image' => 'required',
            'harga' => 'required'
        ]);
    }
}
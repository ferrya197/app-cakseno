<?php
namespace App\Service;

interface IUserService extends IBaseService
{
    public function checkDetailUser($request);
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
        'menu_order_id' => 'array'
    ];

    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu');
    }
}

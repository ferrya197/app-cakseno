<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['role_name', 'access', 'description'];

    protected $casts = [
        'access' => 'json'
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}

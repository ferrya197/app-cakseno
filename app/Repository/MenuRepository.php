<?php
namespace App\Repository;

use App\Models\Menu;

class MenuRepository implements IBaseRepository
{
    public function readAll()
    {
        return Menu::all()->toArray();
    }

    public function create($request)
    {
        $menu = new Menu([
            'category_id' => $request->input('category_id'),
            'menu_name' => $request->input('menu_name'),
            'menu_image' => $request->input('menu_image'),
            'harga' => $request->input('harga'),
            'menu_description' => $request->input('menu_description')
        ]);

        return $menu->save();
    }

    public function read($id)
    {
        return Menu::where('id', $id)->get()->toArray();
        
    }

    public function update($request, $id)
    {
        $menu = Menu::find($id);
        $menu->category_id = $request->input('category_id');
        $menu->menu_name = $request->input('menu_name');
        $menu->menu_image = $request->input('menu_image');
        $menu->harga = $request->input('harga');
        $menu->menu_description = $request->input('menu_description');

        return $menu->save();
    }

    public function delete($id)
    {
        $role = Menu::find($id);

        return $role->delete();
    }
}
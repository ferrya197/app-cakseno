<?php
namespace App\Repository;

use App\User;
use App\Repository\IBaseRepository;
use App\Service\UserService;

class UserRepository implements IBaseRepository
{
    protected $service;

    public function __construct()
    {
        $this->service = new UserService;
    }

    public function readAll()
    {
        return User::all()->toArray();
    }

    public function create($request)
    {
        $user = new User([
            'role_id' => $request->input('role_id'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'detail_user' => $this->service->checkDetailUser($request->input('detail_user'))
        ]);

        return $user->save();
    }

    public function read($id)
    {
        return User::where('id', $id)->get()->toArray();
    }

    public function update($request, $id)
    {
        $user = User::find($id);

        $user->role_id = $request->input('role_id');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->detail_user = $this->service->checkDetailUser($request->input('detail_user'));

        return $user->save();
    }

    public function delete($id)
    {
        $user = User::find($id);
        
        return $user->delete();
    }
}
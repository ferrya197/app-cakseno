<?php
namespace App\Repository;

interface IBaseRepository
{
    public function readAll();
    
    public function create($request);
    
    public function read($id);

    public function update($request, $id);

    public function delete($id);
}
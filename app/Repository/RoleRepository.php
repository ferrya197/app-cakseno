<?php
namespace App\Repository;

use App\Models\Role;
use App\Repository\IBaseRepository;

class RoleRepository implements IBaseRepository
{
    public function readAll()
    {
        return Role::all()->toArray();
    }

    public function create($request)
    {
        $role = new Role([
            'role_name' => $request->input('role_name'),
            'access' => $request->input('access'),
            'description' => $request->input('description')
        ]);

        return $role->save();
    }

    public function read($id)
    {
        return Role::where('id', $id)->get()->toArray();
        
    }

    public function update($request, $id)
    {
        $role = Role::find($id);
        $role->role_name = $request->input('role_name');
        $role->access = $request->input('access');
        $role->description = $request->input('description');

        return $role->save();
    }

    public function delete($id)
    {
        $role = Role::find($id);

        return $role->delete();
    }
}
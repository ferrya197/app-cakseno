<?php
namespace App\Repository;

use App\Models\Category;

class CategoryRepository implements IBaseRepository
{
    public function readAll()
    {
        return Category::all()->toArray();
    }

    public function create($request)
    {
        $category = new Category([
            'category_name' => $request->input('category_name'),
            'description' => $request->input('description')
        ]);

        return $category->save();
    }

    public function read($id)
    {
        return Category::where('id', $id)->get()->toArray();
        
    }

    public function update($request, $id)
    {
        $category = Category::find($id);
        $category->category_name = $request->input('category_name');
        $category->description = $request->input('description');

        return $category->save();
    }

    public function delete($id)
    {
        $category = Category::find($id);

        return $category->delete();
    }
}
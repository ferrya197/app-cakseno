<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function makeResponse($message = null, $data = null)
    {
        if($data == null)
        {
            $response = [
                'msg' => $message
            ];

            return $response;
        }
        else if($data != null && $message != null)
        {
            $response = [
                'msg' => $message,
                'data' => $data
            ];

            return $response;
        }
        else{
            $response = [
                'msg' => null,
                'data' => null
            ];

            return $response;
        }
    }
}

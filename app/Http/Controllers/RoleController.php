<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Service\RoleService;
use Illuminate\Http\Request;
use App\Repository\RoleRepository;

class RoleController extends Controller
{

    protected $service;
    protected $model;

    public function __construct()
    {
        $this->service = new RoleService;
        $this->model = new RoleRepository;
    }

    /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
    public function index()
    {
        $roles = $this->model->readAll();

        $response = $this->service->makeResponse($roles, 'read', 'Role');

        return response()->json($response, 201);
    }

    /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
    public function store(Request $request)
    {
        $this->service->validation($request);

        $checkSaveData = $this->model->create($request);

        $response = $this->service->checkAndCreateResponse($checkSaveData, $request->all(), 'create', 'Role');

        return response()->json($response, 201);
    }

    /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
    public function show($id)
    {
        $role = $this->model->read($id);

        $checkDataIsNotNull = $this->service->checkDataIsExist($role);

        $response = $this->service->checkAndCreateResponse($checkDataIsNotNull, $role, 'read', 'Role');

        return response()->json($response, 201);
    }

    /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
    public function update(Request $request, $id)
    {
        $this->service->validation($request);

        if ($id != 1)
        {
            $checkUpdateData = $this->model->update($request, $id);

            $response = $this->service->checkAndCreateResponse($checkUpdateData, $request->all(), 'update', 'Role');

            return response()->json($response, 201);
        }

        $data = $this->model->read($id);

        $response = $this->service->makeResponse($data, 'forbidden update id', 'Role');

        return response()->json($response, 201);
    }

    /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
    public function destroy($id)
    {
        $data = $this->model->read($id);

        if ($id != 1)
        {
            $checkDeleteData = $this->model->delete($id);

            $response = $this->service->checkAndCreateResponse($checkDeleteData, $data, 'delete', 'Role');

            return response()->json($response, 201);
        }
        
        $response = $this->service->makeResponse($data, 'forbidden delete id', 'Role');

        return response()->json($response, 201);
    }
}

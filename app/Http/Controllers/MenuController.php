<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\MenuRepository;
use App\Service\MenuService;

class MenuController extends Controller
{
    protected $model;
    protected $service;

    public function __construct()
    {
        $this->model = new MenuRepository;
        $this->service = new MenuService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = $this->model->readAll();

        $response = $this->service->makeResponse($menus, 'read', 'Menu');

        return response()->json($response, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->service->validation($request);

        $checkSaveData = $this->model->create($request);

        $response = $this->service->checkAndCreateResponse($checkSaveData, $request->all(), 'create', 'Menu');

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = $this->model->read($id);

        $checkDataIsNotNull = $this->service->checkDataIsExist($menu);

        $response = $this->service->checkAndCreateResponse($checkDataIsNotNull, $menu, 'read', 'Menu');

        return response()->json($response, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->service->validation($request);

        $checkUpdateData = $this->model->update($request, $id);

        $response = $this->service->checkAndCreateResponse($checkUpdateData, $request->all(), 'update', 'Menu');

        return response()->json($response, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->model->read($id);

        $checkDeleteData = $this->model->delete($id);

        $response = $this->service->checkAndCreateResponse($checkDeleteData, $data, 'delete', 'Menu');

        return response()->json($response, 201);
    }
}

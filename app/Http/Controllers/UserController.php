<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Repository\UserRepository;
use App\Service\UserService;

class UserController extends Controller
{
    protected $model;
    protected $service;

    public function __construct(){
        $this->model = new UserRepository;
        $this->service = new UserService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->model->readAll();

        $response = $this->service->makeResponse($users, 'read', 'User');

        return response()->json($response, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->service->validation($request);

        $checkSaveData = $this->model->create($request);

        $response = $this->service->checkAndCreateResponse($checkSaveData, $request->all(), 'create', 'User');

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->model->read($id);

        $checkDataIsNotNull = $this->service->checkDataIsExist($user);

        $response = $this->service->checkAndCreateResponse($checkDataIsNotNull, $user, 'read', 'User');

        return response()->json($response, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->service->validation($request);

        $checkUpdateData = $this->model->update($request, $id);

        $response = $this->service->checkAndCreateResponse($checkUpdateData, $request->all(), 'update', 'User');

        return response()->json($response, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->model->read($id);

        $checkDeleteData = $this->model->delete($id);

        $response = $this->service->checkAndCreateResponse($checkDeleteData, $user, 'delete', 'User');

        return response()->json($response, 201);        
    }
}

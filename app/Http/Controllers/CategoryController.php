<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Repository\CategoryRepository;
use App\Service\CategoryService;

class CategoryController extends Controller
{
    protected $model;
    protected $service;

    public function __construct()
    {
        $this->model = new CategoryRepository;
        $this->service = new CategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->model->readAll();

        $response = $this->service->makeResponse($categories, 'read', 'Category');

        return response()->json($response, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->service->validation($request);

        $checkSaveData = $this->model->create($request);

        $response = $this->service->checkAndCreateResponse($checkSaveData, $request->all(), 'create', 'Category');

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->model->read($id);

        $checkDataIsNotNull = $this->service->checkDataIsExist($category);

        $response = $this->service->checkAndCreateResponse($checkDataIsNotNull, $category, 'read', 'Category');

        return response()->json($response, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->service->validation($request);

        $checkUpdateData = $this->model->update($request, $id);

        $response = $this->service->checkAndCreateResponse($checkUpdateData, $request->all(), 'update', 'Category');

        return response()->json($response, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->model->read($id);

        $checkDeleteCategory = $this->model->delete($id);

        $response = $this->service->checkAndCreateResponse($checkDeleteCategory, $category, 'delete', 'Category');

        return response()->json($response, 201);
    }
}

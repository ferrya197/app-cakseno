<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestUploadController extends Controller
{
    public function index(Request $request)
    {
        $file = $request->file('abc');
        $fileName = $request->input('test');
        $fileExtension = $file->extension();

        $upload = $file->storeAs('images/menu', $fileName . '.' . $fileExtension);

        if($upload)
        {
            return response()->json($upload, 201);
        }
        // return response()->json($filename->getFilename(), 201);
        return dd($filename);
        // return response()->json($request->all(), 201);
    }
}

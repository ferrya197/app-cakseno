import TitleComponent from './components/TitlePage.vue';
import InfoCardComponent from './components/InfoCardComponent.vue';

export default{
    components: {
        'my-title' : TitleComponent,
        'my-info' : InfoCardComponent
    }
}
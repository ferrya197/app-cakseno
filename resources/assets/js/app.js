require('./bootstrap');
import Vue from 'vue';
import router from './router';
import MainApp from './view/index.vue';
import store from './store';
import mixin from './mixins';

Vue.mixin(mixin);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        'main-app' : MainApp
    }
});

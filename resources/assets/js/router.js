import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store';

import Home from './view/home/home.vue';

// role view
import Role from './view/role/index.vue';
import RoleDefault from './view/role/default.vue';

import Login from './view/auth/AuthLogin.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/role',
            component: Role,
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: '/',
                    name: 'role',
                    component: RoleDefault
                }
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        }
    ] 
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = store.state.currentUser;

    if(requiresAuth && !currentUser)
    {
        next('/login');
    }
    else if(to.path == '/login' && currentUser)
    {
        next('/');
    }
    else
    {
        next();
    }
});

export default router
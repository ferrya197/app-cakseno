import Vue from 'vue';
import Vuex from 'vuex';
import { getLocalUser } from './helpers/auth';

Vue.use(Vuex);

const user = getLocalUser();

const store = new Vuex.Store({
    state: {
        currentUser: user,
        isloggedIn: !!user,
        loading: false,
        auth_error: null,
    },
    getters: {
        isLoading(state){
            return state.loading;
        },
        isloggedIn(state){
            return state.isloggedIn;
        },
        currentUser(state){
            return state.currentUser;
        },
        authError(state){
            return state.auth_error;
        }
    },
    mutations: {
        login(state){
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload){
            state.auth_error = null;
            state.isloggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.data, {token: payload.data.api_token});

            localStorage.setItem('user', JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload){
            state.loading = false;
            state.auth_error = payload.error;
        },
        logout(state){
            localStorage.removeItem('user');
            state.isloggedIn = false;
            state.currentUser = null;
        }
    },
    actions: {
        login(context){
            context.commit('login');
        }
    }
})

export default store;